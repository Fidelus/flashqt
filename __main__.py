from PyQt5.QtWidgets import QApplication, QWidget
from PyQt5.Qt import *
from PyQt5 import QtWidgets
import sys
import qdarkgraystyle

class App(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("flashQT")
        self.create_ui()
        self.setLayout(self.main_layout)
        self.counter = 0

    def create_ui(self):
        #Create layouts
        self.main_layout = QVBoxLayout()
        self.run_button = QPushButton('Run')

        # TODO
        # self.run_button.clicked.connect(self.start_game)
        self.main_layout.addWidget(self.run_button)


        self.game_layout = QHBoxLayout()
        self.buttons_layout = QHBoxLayout()
        self.settings_layout = QVBoxLayout()

        self.main_layout.addLayout(self.game_layout)
        self.game_layout.addLayout(self.buttons_layout)
        self.game_layout.addLayout(self.settings_layout)

        # Create button
        self.button = QPushButton('Click me')
        self.buttons_layout.addWidget(self.button)
        self.button.clicked.connect(self.button_clicked)

        # Create points view
        self.points_view = QtWidgets.QLabel()
        self.settings_layout.addWidget(self.points_view)

    def button_clicked(self):
        self.counter += 1
        self.points_view.setText(str(self.counter))

    # TODO
    # def start_game(self):


def main():
    app = QApplication(sys.argv)
    main_window = App()
    main_window.show()
    app.setStyleSheet(qdarkgraystyle.load_stylesheet())

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
